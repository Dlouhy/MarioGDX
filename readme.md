Mario Bros.
=============

Jedná se o jednoduchou hru na způsob známé hry Mario Bros. Hra je vyvíjena pomocí knihovny [libGDX](https://libgdx.badlogicgames.com/), která je multiplatformní a open source.

Hra bude poskytovat možnost podívat se za jaký čas uživatel zvládl úroveň. Dále bude možné porovnávat své výsledky s ostatními hráči, protože se výsledky budou online ukládat na server, kde budou veřejně přístupné všem hráčům.

Vývojářský deník
-----------------
1) V této fázi je první Initial commit. Zde je nahraná mapa, která byla vytvořena pomocí nástroje [Tiled](http://www.mapeditor.org/).