package com.kubitini.mario.Scenes;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Disableable;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.kubitini.mario.Mario;
import com.kubitini.mario.Screens.PlayScreen;
import com.kubitini.mario.Sprites.MarioSprite;

import java.util.Locale;

/**
 * Created by Kubitini on 9. 12. 2017.
 */

public class Hud implements Disposable {
    public Stage stage;

    private Viewport viewport;

    private Integer worldTimer;
    private float timeCount;
    private static Integer score;

    Label countdownLabel;
    static Label scoreLabel;
    Label timeLabel;
    Label levelLabel;
    Label worldLabel;
    Label marioLabel;

    private MarioSprite mario;

    public Hud(SpriteBatch sb, MarioSprite mario) {
        this.mario = mario;
        worldTimer = 300;
        timeCount = 0;
        score = 0;

        viewport = new FitViewport(Mario.V_WIDTH, Mario.V_HEIGHT, new OrthographicCamera());
        stage = new Stage(viewport, sb);

        Table table = new Table();
        table.top();
        table.setFillParent(true);

        countdownLabel = new Label(
                String.format(
                        Locale.US,
                        "%03d",
                        worldTimer
                ),
                new Label.LabelStyle(
                        new BitmapFont(),
                        Color.WHITE
                )
        );

        scoreLabel = new Label(
                String.format(
                        Locale.US,
                        "%06d",
                        score
                ),
                new Label.LabelStyle(
                        new BitmapFont(),
                        Color.WHITE
                )
        );

        timeLabel = new Label(
                "TIME",
                new Label.LabelStyle(
                        new BitmapFont(),
                        Color.WHITE
                )
        );

        levelLabel = new Label(
                "1-1",
                new Label.LabelStyle(
                        new BitmapFont(),
                        Color.WHITE
                )
        );

        worldLabel = new Label(
                "WORLD",
                new Label.LabelStyle(
                        new BitmapFont(),
                        Color.WHITE
                )
        );

        marioLabel = new Label(
                "MARIO",
                new Label.LabelStyle(
                        new BitmapFont(),
                        Color.WHITE
                )
        );

        table.add(marioLabel).expandX().padTop(10);
        table.add(worldLabel).expandX().padTop(10);
        table.add(timeLabel).expandX().padTop(10);
        table.row();
        table.add(scoreLabel).expandX();
        table.add(levelLabel).expandX();
        table.add(countdownLabel).expandX();

        stage.addActor(table);
    }

    public void update(float dt) {
        timeCount += dt;

        if(timeCount > 1 && !mario.isDead()) {
            worldTimer--;
            if(worldTimer < 0 && !mario.isDead()) {
                mario.killMario();
            }
            else {
                countdownLabel.setText(
                        String.format(
                                Locale.US,
                                "%03d",
                                worldTimer
                        )
                );
            }

            timeCount = 0;
        }
    }

    // TODO: Tip for future: Try to make it non-static
    public static void addScore(int value) {
        score += value;
        scoreLabel.setText(
                String.format(
                        Locale.US,
                        "%06d",
                        score
                )
        );
    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
