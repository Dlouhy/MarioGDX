package com.kubitini.mario.Scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.kubitini.mario.Mario;

/**
 * Created by Kubitini on 21. 12. 2017.
 */

public class Controller implements Disposable {
    private Viewport viewport;
    private Stage stage;
    private OrthographicCamera camera;
    private boolean jumpPressed;
    private boolean rightPressed;
    private boolean leftPressed;

    public Controller(SpriteBatch batch) {
        camera = new OrthographicCamera();
        viewport = new FitViewport(Mario.V_WIDTH, Mario.V_HEIGHT, camera);
        stage = new Stage(viewport, batch);
        Gdx.input.setInputProcessor(stage);

        Table table = new Table();
        table.right().bottom();
        table.setFillParent(true);

        Table tableJump = new Table();
        tableJump.left().bottom();
        tableJump.setFillParent(true);

        Image jumpImg = new Image(new Texture("up.png"));
        jumpImg.setSize(20, 20);
        jumpImg.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                jumpPressed = true;
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                jumpPressed = false;
            }
        });

        Image leftImg = new Image(new Texture("left.png"));
        leftImg.setSize(20, 20);
        leftImg.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                leftPressed = true;
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                leftPressed = false;
            }
        });

        Image rightImg = new Image(new Texture("right.png"));
        rightImg.setSize(20, 20);
        rightImg.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                rightPressed = true;
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                rightPressed = false;
            }
        });

        table
                .add(leftImg)
                .size(leftImg.getWidth(), leftImg.getHeight())
                .padBottom(10);
        table
                .add()
                .size(leftImg.getWidth(), leftImg.getHeight())
                .padBottom(10);
        table
                .add(rightImg)
                .size(rightImg.getWidth(), rightImg.getHeight())
                .padRight(20)
                .padBottom(10);

        tableJump
                .add(jumpImg)
                .size(jumpImg.getWidth(), jumpImg.getHeight())
                .padLeft(20)
                .padBottom(10);

        stage.addActor(table);
        stage.addActor(tableJump);
    }

    public void draw() {
        stage.draw();
    }

    public boolean isJumpPressed() {
        return jumpPressed;
    }

    public boolean isRightPressed() {
        return rightPressed;
    }

    public boolean isLeftPressed() {
        return leftPressed;
    }

    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    public Camera getCamera() {
        return stage.getCamera();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
