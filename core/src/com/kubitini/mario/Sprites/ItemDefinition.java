package com.kubitini.mario.Sprites;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by Kubitini on 20. 12. 2017.
 */

public class ItemDefinition {
    public Vector2 position;
    public Class<?> type;

    public ItemDefinition(Vector2 position, Class<?> type) {
        this.position = position;
        this.type = type;
    }
}
