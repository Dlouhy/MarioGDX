package com.kubitini.mario.Sprites;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.kubitini.mario.Mario;
import com.kubitini.mario.Screens.PlayScreen;

/**
 * Created by Kubitini on 20. 12. 2017.
 */

public class Mushroom extends Item {
    public Mushroom(PlayScreen screen, float x, float y) {
        super(screen, x, y);
        setRegion(screen.getAtlas().findRegion("mushroom"), 0, 0, 16, 16);
        velocity = new Vector2(0.7f, 0);
    }

    @Override
    public void defineItem() {
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(getX(), getY());
        bodyDef.type = BodyDef.BodyType.DynamicBody;

        b2body = world.createBody(bodyDef);

        FixtureDef fixtureDef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(6 / Mario.PixelsPerMeter);
        fixtureDef.shape = shape;
        fixtureDef.filter.categoryBits = Mario.ITEM_BIT;
        fixtureDef.filter.maskBits = Mario.MARIO_BIT |
                Mario.OBJECT_BIT |
                Mario.GROUND_BIT |
                Mario.COIN_BIT |
                Mario.BRICK_BIT;

        b2body.createFixture(fixtureDef).setUserData(this);
    }

    @Override
    public void use(MarioSprite mario) {
        destroy();
        mario.grow();
    }

    public void update(float dt) {
        super.update(dt);

        setPosition(
                b2body.getPosition().x - getWidth() / 2,
                b2body.getPosition().y - getHeight() / 2
        );

        velocity.y = b2body.getLinearVelocity().y;
        b2body.setLinearVelocity(velocity);
    }
}
