package com.kubitini.mario.Sprites;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Array;
import com.kubitini.mario.Mario;
import com.kubitini.mario.Screens.PlayScreen;
import com.sun.org.apache.xerces.internal.impl.xpath.XPath;

/**
 * Created by Kubitini on 20. 12. 2017.
 */

public class Turtle extends Enemy {
    public static final int KICK_LEFT_SPEED = -2;
    public static final int KICK_RIGHT_SPEED = 2;

    public enum State { WALKING, STANDING_SHELL, MOVING_SHELL, DEAD }
    public State currentState;
    public State previousState;
    private float stateTime;
    private Animation<TextureRegion> walkAnimation;
    private Array<TextureRegion> frames;
    private TextureRegion shell;
    private float deadRotationDegrees;

    public Turtle(PlayScreen screen, float x, float y) {
        super(screen, x, y);

        frames = new Array<TextureRegion>();
        frames.add(
                new TextureRegion(
                        screen.getAtlas().findRegion("turtle"),
                        0,
                        0,
                        16,
                        24
                )
        );
        frames.add(
                new TextureRegion(
                        screen.getAtlas().findRegion("turtle"),
                        16,
                        0,
                        16,
                        24
                )
        );

        walkAnimation = new Animation<TextureRegion>(0.2f, frames);
        frames.clear();

        shell = new TextureRegion(
                screen.getAtlas().findRegion("turtle"),
                64,
                0,
                16,
                24
        );

        currentState = previousState = State.WALKING;

        setBounds(
                getX(),
                getY(),
                16 / Mario.PixelsPerMeter,
                24 / Mario.PixelsPerMeter
        );

        deadRotationDegrees = 0;
    }

    @Override
    protected void defineEnemy() {
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(getX(), getY());
        bodyDef.type = BodyDef.BodyType.DynamicBody;

        b2body = world.createBody(bodyDef);

        FixtureDef fixtureDef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(6 / Mario.PixelsPerMeter);
        fixtureDef.shape = shape;
        fixtureDef.filter.categoryBits = Mario.ENEMY_BIT;
        fixtureDef.filter.maskBits = Mario.BRICK_BIT |
                Mario.COIN_BIT |
                Mario.GROUND_BIT |
                Mario.ENEMY_BIT |
                Mario.OBJECT_BIT |
                Mario.MARIO_BIT;

        b2body.createFixture(fixtureDef).setUserData(this);

        PolygonShape head = new PolygonShape();
        Vector2[] verticies = new Vector2[4];
        verticies[0] = new Vector2(-5, 8).scl(1 / Mario.PixelsPerMeter);
        verticies[1] = new Vector2(5, 8).scl(1 / Mario.PixelsPerMeter);
        verticies[2] = new Vector2(-3, 3).scl(1 / Mario.PixelsPerMeter);
        verticies[3] = new Vector2(3, 3).scl(1 / Mario.PixelsPerMeter);
        head.set(verticies);

        fixtureDef.shape = head;
        fixtureDef.restitution = 1.5f;
        fixtureDef.filter.categoryBits = Mario.ENEMY_HEAD_BIT;

        b2body.createFixture(fixtureDef).setUserData(this);
    }

    @Override
    public void onHitHead(MarioSprite mario) {
        if(currentState != State.STANDING_SHELL) {
            currentState = State.STANDING_SHELL;
            velocity.x = 0;
        }
        else {
            kick(mario.getX() <= this.getX() ? KICK_RIGHT_SPEED : KICK_LEFT_SPEED);
        }
    }

    @Override
    public void update(float dt) {
        setRegion(getFrame(dt));

        if(currentState == State.STANDING_SHELL && stateTime > 5) {
            currentState = State.WALKING;
            velocity.x = 1;
        }

        setPosition(
                b2body.getPosition().x - getWidth() / 2,
                b2body.getPosition().y - 8 / Mario.PixelsPerMeter
        );

        if(currentState == State.DEAD) {
            deadRotationDegrees += 3;
            rotate(deadRotationDegrees);

            if(stateTime > 5 && !isDestroyed) {
                world.destroyBody(b2body);
                isDestroyed = true;
            }
        }
        else {
            b2body.setLinearVelocity(velocity);
        }
    }

    public TextureRegion getFrame(float dt) {
        TextureRegion region;

        switch (currentState) {
            case MOVING_SHELL:
            case STANDING_SHELL:
                region = shell;
                break;
            case WALKING:
            default:
                region = walkAnimation.getKeyFrame(stateTime, true);
                break;
        }

        if(velocity.x > 0 && !region.isFlipX()) {
            region.flip(true, false);
        }
        if(velocity.x < 0 && region.isFlipX()) {
            region.flip(true, false);
        }

        stateTime = currentState == previousState ? stateTime + dt : 0;
        previousState = currentState;

        return region;
    }

    public void kick(int speed) {
        velocity.x = speed;
        currentState = State.MOVING_SHELL;
    }

    public State getCurrentState() {
        return currentState;
    }

    public void onEnemyHit(Enemy enemy) {
        if(enemy instanceof Turtle) {
            if(((Turtle) enemy).getCurrentState() == State.MOVING_SHELL && currentState != State.MOVING_SHELL) {
                killed();
            }
            else if(currentState == State.MOVING_SHELL && ((Turtle) enemy).getCurrentState() == State.WALKING) {
                return;
            }
            else {
                reverseVelocity(true, false);
            }
        }
        else if(currentState != State.MOVING_SHELL) {
            reverseVelocity(true, false);
        }
        else {
            reverseVelocity(true, false);
        }
    }

    public void killed() {
        currentState = State.DEAD;
        Filter filter = new Filter();
        filter.maskBits = Mario.NOTHING_BIT;

        for(Fixture fixture : b2body.getFixtureList()) {
            fixture.setFilterData(filter);
        }

        b2body.applyLinearImpulse(new Vector2(0, 5f), b2body.getWorldCenter(), true);
    }
}
