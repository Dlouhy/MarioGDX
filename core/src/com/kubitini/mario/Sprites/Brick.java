package com.kubitini.mario.Sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.World;
import com.kubitini.mario.Mario;
import com.kubitini.mario.Scenes.Hud;
import com.kubitini.mario.Screens.PlayScreen;

/**
 * Created by Kubitini on 9. 12. 2017.
 */

public class Brick extends InteractiveTileObject {
    public Brick(PlayScreen screen, MapObject object) {
        super(screen, object);

        fixture.setUserData(this);
        setCategoryFilter(Mario.BRICK_BIT);
    }

    @Override
    public void onHeadHit(MarioSprite mario) {
        if(mario.isBig()) {
            Gdx.app.log("Brick", "Hit");
            setCategoryFilter(Mario.DESTROYED_BIT);
            getCell().setTile(null);
            Hud.addScore(100);
            Mario.manager.get("audio/sounds/breakblock.wav", Sound.class).play();
        }
        else {
            Mario.manager.get("audio/sounds/bump.wav", Sound.class).play();
        }
    }
}
