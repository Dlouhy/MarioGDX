package com.kubitini.mario.Sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.kubitini.mario.Mario;
import com.kubitini.mario.Scenes.Hud;
import com.kubitini.mario.Screens.PlayScreen;

/**
 * Created by Kubitini on 9. 12. 2017.
 */

public class Coin extends InteractiveTileObject {
    private static TiledMapTileSet tileSet;
    private final int BLANK_COIN = 28;

    public Coin(PlayScreen screen, MapObject object) {
        super(screen, object);

        tileSet = map.getTileSets().getTileSet("tileset_gutter");

        fixture.setUserData(this);
        setCategoryFilter(Mario.COIN_BIT);
    }

    @Override
    public void onHeadHit(MarioSprite mario) {
        Gdx.app.log("Coin", "Hit");

        if(getCell().getTile().getId() == BLANK_COIN) {
            Mario.manager.get("audio/sounds/bump.wav", Sound.class).play();
        }
        else {
            if(object.getProperties().containsKey("mushroom")) {
                screen.spawnItem(
                        new ItemDefinition(
                                new Vector2(
                                        body.getPosition().x,
                                        body.getPosition().y + 16 / Mario.PixelsPerMeter),
                                Mushroom.class
                        )
                );

                Mario.manager.get("audio/sounds/powerup_spawn.wav", Sound.class).play();
            }
            else {
                Mario.manager.get("audio/sounds/coin.wav", Sound.class).play();
                Hud.addScore(200);
            }
        }

        getCell().setTile(tileSet.getTile(BLANK_COIN));
    }
}
