package com.kubitini.mario.Sprites;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.kubitini.mario.Mario;
import com.kubitini.mario.Screens.PlayScreen;

/**
 * Created by Kubitini on 20. 12. 2017.
 */

public abstract class Item extends Sprite {
    protected PlayScreen screen;
    protected World world;
    protected Vector2 velocity;
    protected boolean toBeDestroyed;
    protected boolean isDestroyed;
    protected Body b2body;

    public Item(PlayScreen screen, float x, float y) {
        this.screen = screen;
        this.world = screen.getWorld();

        setPosition(x, y);
        setBounds(
                getX(),
                getY(),
                16 / Mario.PixelsPerMeter,
                16 / Mario.PixelsPerMeter
        );

        defineItem();
        toBeDestroyed = false;
        isDestroyed = false;
    }

    public abstract void defineItem();
    public abstract void use(MarioSprite mario);

    public void update(float dt) {
        if(toBeDestroyed && !isDestroyed) {
            world.destroyBody(b2body);
            isDestroyed = true;
        }
    }

    public void draw(Batch batch) {
        if(!isDestroyed) {
            super.draw(batch);
        }
    }

    public void destroy() {
        toBeDestroyed = true;
    }

    public void reverseVelocity(boolean x, boolean y) {
        if(x) {
            velocity.x = -velocity.x;
        }

        if(y) {
            velocity.y = -velocity.y;
        }
    }
}
