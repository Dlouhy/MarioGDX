package com.kubitini.mario.Sprites;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.kubitini.mario.Screens.PlayScreen;

/**
 * Created by Kubitini on 19. 12. 2017.
 */

public abstract class Enemy extends Sprite {
    protected World world;
    protected PlayScreen screen;
    public Body b2body;
    protected boolean isDestroyed;
    protected boolean toBeDestroyed;
    public Vector2 velocity;

    public Enemy(PlayScreen screen, float x, float y) {
        this.world = screen.getWorld();
        this.screen = screen;
        this.isDestroyed = false;
        this.toBeDestroyed = false;
        velocity = new Vector2(1, 0);

        setPosition(x, y);
        defineEnemy();

        b2body.setActive(false);
    }

    protected abstract void defineEnemy();
    public abstract void onHitHead(MarioSprite mario);
    public abstract void update(float dt);
    public abstract void onEnemyHit(Enemy enemy);

    public void reverseVelocity(boolean x, boolean y) {
        if(x) {
            velocity.x = -velocity.x;
        }

        if(y) {
            velocity.y = -velocity.y;
        }
    }
}
