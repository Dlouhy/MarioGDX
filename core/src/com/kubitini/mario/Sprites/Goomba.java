package com.kubitini.mario.Sprites;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Array;
import com.kubitini.mario.Mario;
import com.kubitini.mario.Screens.PlayScreen;

/**
 * Created by Kubitini on 19. 12. 2017.
 */

public class Goomba extends Enemy {
    private float stateTime;
    private Animation<TextureRegion> walkAnimation;
    private Array<TextureRegion> frames;

    public Goomba(PlayScreen screen, float x, float y) {
        super(screen, x, y);

        frames = new Array<TextureRegion>();
        for(int i = 0; i < 2; i++) {
            frames.add(new TextureRegion(screen.getAtlas().findRegion("goomba"), i * 16, 0, 16, 16));
        }
        walkAnimation = new Animation<TextureRegion>(0.4f, frames);
        stateTime = 0;

        setBounds(
                getX(),
                getY(),
                16 / Mario.PixelsPerMeter,
                16 / Mario.PixelsPerMeter
        );
    }

    @Override
    protected void defineEnemy() {
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(getX(), getY());
        bodyDef.type = BodyDef.BodyType.DynamicBody;

        b2body = world.createBody(bodyDef);

        FixtureDef fixtureDef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(6 / Mario.PixelsPerMeter);
        fixtureDef.shape = shape;
        fixtureDef.filter.categoryBits = Mario.ENEMY_BIT;
        fixtureDef.filter.maskBits = Mario.BRICK_BIT |
                Mario.COIN_BIT |
                Mario.GROUND_BIT |
                Mario.ENEMY_BIT |
                Mario.OBJECT_BIT |
                Mario.MARIO_BIT;

        b2body.createFixture(fixtureDef).setUserData(this);

        PolygonShape head = new PolygonShape();
        Vector2[] verticies = new Vector2[4];
        verticies[0] = new Vector2(-5, 8).scl(1 / Mario.PixelsPerMeter);
        verticies[1] = new Vector2(5, 8).scl(1 / Mario.PixelsPerMeter);
        verticies[2] = new Vector2(-3, 3).scl(1 / Mario.PixelsPerMeter);
        verticies[3] = new Vector2(3, 3).scl(1 / Mario.PixelsPerMeter);
        head.set(verticies);

        fixtureDef.shape = head;
        fixtureDef.restitution = 0.5f;
        fixtureDef.filter.categoryBits = Mario.ENEMY_HEAD_BIT;

        b2body.createFixture(fixtureDef).setUserData(this);
    }

    public void update(float dt) {
        stateTime += dt;

        if(toBeDestroyed && !isDestroyed) {
            world.destroyBody(b2body);
            isDestroyed = true;
            setRegion(new TextureRegion(screen.getAtlas().findRegion("goomba"), 32, 0, 16, 16));
            stateTime = 0;
        }
        else if(!isDestroyed) {
            b2body.setLinearVelocity(velocity);
            setPosition(
                    b2body.getPosition().x - getWidth() / 2,
                    b2body.getPosition().y - getHeight() / 2
            );

            setRegion(walkAnimation.getKeyFrame(stateTime, true));
        }
    }

    public void draw(Batch batch) {
        if(!isDestroyed || stateTime < 1) {
            super.draw(batch);
        }
    }

    @Override
    public void onHitHead(MarioSprite mario) {
        toBeDestroyed = true;
        Mario.manager.get("audio/sounds/stomp.wav", Sound.class).play();
    }

    public void onEnemyHit(Enemy enemy) {
        if(enemy instanceof Turtle && ((Turtle) enemy).getCurrentState() == Turtle.State.MOVING_SHELL) {
            toBeDestroyed = true;
        }
        else {
            reverseVelocity(true, false);
        }
    }
}
