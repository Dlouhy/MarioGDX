package com.kubitini.mario.Sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.kubitini.mario.Mario;
import com.kubitini.mario.Scenes.Hud;
import com.kubitini.mario.Screens.PlayScreen;

import java.util.Locale;

/**
 * Created by Kubitini on 9. 12. 2017.
 */

public class MarioSprite extends Sprite {
    public World world;
    public Body b2body;


    public enum State { FALLING, JUMPING, STANDING, RUNNING, GROWING, DEAD };
    public State currentState;
    public State previousState;
    private TextureRegion marioStand;
    private TextureRegion bigMarioStand;
    private TextureRegion marioJump;
    private TextureRegion bigMarioJump;
    private TextureRegion deadMario;
    private Animation<TextureRegion> runAnimation;
    private Animation<TextureRegion> bigRunAnimation;
    private Animation<TextureRegion> growMario;
    private float stateTimer;
    private boolean isRunningRight;
    private boolean marioIsBig;
    private boolean runGrowAnimation;
    private boolean timeToDefineBigMario;
    private boolean timeToRedefineMario;
    private boolean isMarioDead;
    private boolean hasWon;

    public MarioSprite(PlayScreen screen) {
        this.world = screen.getWorld();
        currentState = State.STANDING;
        previousState = State.STANDING;
        isRunningRight = true;
        stateTimer = 0;

        Array<TextureRegion> frames = new Array<TextureRegion>();
        for(int i = 1; i < 4; i++) {
            frames.add(new TextureRegion(screen.getAtlas().findRegion("little_mario"), i * 16, 0, 16, 16));
        }
        runAnimation = new Animation<TextureRegion>(0.1f, frames);
        frames.clear();

        for(int i = 1; i < 4; i++) {
            frames.add(new TextureRegion(screen.getAtlas().findRegion("big_mario"), i * 16, 0, 16, 32));
        }
        bigRunAnimation = new Animation<TextureRegion>(0.1f, frames);
        frames.clear();

        marioJump = new TextureRegion(screen.getAtlas().findRegion("little_mario"), 80, 0, 16, 16);
        bigMarioJump = new TextureRegion(screen.getAtlas().findRegion("big_mario"), 80, 0, 16, 32);

        deadMario = new TextureRegion(screen.getAtlas().findRegion("little_mario"), 96, 0, 16, 16);

        marioStand = new TextureRegion(screen.getAtlas().findRegion("little_mario"), 0, 0, 16, 16);
        bigMarioStand = new TextureRegion(screen.getAtlas().findRegion("big_mario"), 0, 0, 16, 32);

        frames.add(new TextureRegion(screen.getAtlas().findRegion("big_mario"), 240, 0, 16, 32));
        frames.add(new TextureRegion(screen.getAtlas().findRegion("big_mario"), 0, 0, 16, 32));
        frames.add(new TextureRegion(screen.getAtlas().findRegion("big_mario"), 240, 0, 16, 32));
        frames.add(new TextureRegion(screen.getAtlas().findRegion("big_mario"), 0, 0, 16, 32));

        growMario = new Animation<TextureRegion>(0.2f, frames);
        frames.clear();

        setBounds(
                0,
                0,
                16 / Mario.PixelsPerMeter,
                16 / Mario.PixelsPerMeter
        );
        setRegion(marioStand);

        defineMario();

        hasWon = false;
    }

    private void defineMario() {
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(
                32 / Mario.PixelsPerMeter,
                32 / Mario.PixelsPerMeter
        );
        bodyDef.type = BodyDef.BodyType.DynamicBody;

        b2body = world.createBody(bodyDef);

        FixtureDef fixtureDef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(6 / Mario.PixelsPerMeter);
        fixtureDef.shape = shape;
        fixtureDef.filter.categoryBits = Mario.MARIO_BIT;
        fixtureDef.filter.maskBits = Mario.BRICK_BIT |
                Mario.COIN_BIT |
                Mario.GROUND_BIT |
                Mario.OBJECT_BIT |
                Mario.ENEMY_BIT |
                Mario.ENEMY_HEAD_BIT |
                Mario.ITEM_BIT |
                Mario.FINISH_BIT;

        b2body.createFixture(fixtureDef).setUserData(this);

        EdgeShape head = new EdgeShape();
        head.set(
                new Vector2(
                        -2 / Mario.PixelsPerMeter,
                        6 / Mario.PixelsPerMeter),
                new Vector2(
                        2 / Mario.PixelsPerMeter,
                        6 / Mario.PixelsPerMeter)
        );

        fixtureDef.shape = head;
        fixtureDef.isSensor = true;
        fixtureDef.filter.categoryBits = Mario.MARIO_HEAD_BIT;

        b2body.createFixture(fixtureDef).setUserData(this);
    }

    private void defineBigMario() {
        Vector2 currentPosition = b2body.getPosition();
        world.destroyBody(b2body);

        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(currentPosition.add(0, 10 / Mario.PixelsPerMeter));
        bodyDef.type = BodyDef.BodyType.DynamicBody;

        b2body = world.createBody(bodyDef);

        FixtureDef fixtureDef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(6 / Mario.PixelsPerMeter);
        fixtureDef.shape = shape;
        fixtureDef.filter.categoryBits = Mario.MARIO_BIT;
        fixtureDef.filter.maskBits = Mario.BRICK_BIT |
                Mario.COIN_BIT |
                Mario.GROUND_BIT |
                Mario.OBJECT_BIT |
                Mario.ENEMY_BIT |
                Mario.ENEMY_HEAD_BIT |
                Mario.ITEM_BIT |
                Mario.FINISH_BIT;

        b2body.createFixture(fixtureDef).setUserData(this);
        shape.setPosition(new Vector2(0, -14 / Mario.PixelsPerMeter));
        b2body.createFixture(fixtureDef).setUserData(this);

        EdgeShape head = new EdgeShape();
        head.set(
                new Vector2(
                        -2 / Mario.PixelsPerMeter,
                        6 / Mario.PixelsPerMeter),
                new Vector2(
                        2 / Mario.PixelsPerMeter,
                        6 / Mario.PixelsPerMeter)
        );

        fixtureDef.shape = head;
        fixtureDef.isSensor = true;
        fixtureDef.filter.categoryBits = Mario.MARIO_HEAD_BIT;

        b2body.createFixture(fixtureDef).setUserData(this);
        timeToDefineBigMario = false;
    }

    public void update(float dt) {
        if(b2body.getPosition().y < 0 && !isDead()) {
            killMario();
        }

        if(marioIsBig) {
            setPosition(
                    b2body.getPosition().x - getWidth() / 2,
                    b2body.getPosition().y - getHeight() / 2 - 6 / Mario.PixelsPerMeter
            );
        }
        else {
            setPosition(
                    b2body.getPosition().x - getWidth() / 2,
                    b2body.getPosition().y - getHeight() / 2
            );
        }

        setRegion(getFrame(dt));

        if(timeToDefineBigMario) {
            defineBigMario();
        }

        if(timeToRedefineMario) {
            redefineMario();
        }
    }

    private TextureRegion getFrame(float dt) {
        currentState = getState();
        TextureRegion region;

        switch (currentState) {
            case DEAD:
                region = deadMario;
                break;
            case GROWING:
                region = growMario.getKeyFrame(stateTimer);
                if(growMario.isAnimationFinished(stateTimer)) {
                    runGrowAnimation = false;
                }
                break;
            case JUMPING:
                region = marioIsBig ? bigMarioJump : marioJump;
                break;
            case RUNNING:
                region = marioIsBig ? bigRunAnimation.getKeyFrame(stateTimer, true) : runAnimation.getKeyFrame(stateTimer, true);
                break;
            case FALLING:
            case STANDING:
            default:
                region = marioIsBig ? bigMarioStand : marioStand;
                break;
        }

        if((b2body.getLinearVelocity().x < 0 || !isRunningRight) && !region.isFlipX()) {
            region.flip(true, false);
            isRunningRight = false;
        }
        else if((b2body.getLinearVelocity().x > 0 || isRunningRight) && region.isFlipX()) {
            region.flip(true, false);
            isRunningRight = true;
        }

        stateTimer = currentState == previousState ? stateTimer + dt : 0;
        previousState = currentState;

        return region;
    }

    public State getState() {
        if(isMarioDead) {
            return State.DEAD;
        }
        else if(runGrowAnimation) {
            return State.GROWING;
        }
        else if(b2body.getLinearVelocity().y > 0 || (b2body.getLinearVelocity().y < 0 && previousState == State.JUMPING)) {
            return State.JUMPING;
        }
        else if(b2body.getLinearVelocity().y < 0) {
            return State.FALLING;
        }
        else if(b2body.getLinearVelocity().x != 0) {
            return State.RUNNING;
        }
        else {
            return  State.STANDING;
        }
    }

    public void grow() {
        if(!marioIsBig) {
            runGrowAnimation = true;
            marioIsBig = true;
            timeToDefineBigMario = true;
            setBounds(getX(), getY(), getWidth(), getHeight() * 2);
            Mario.manager.get("audio/sounds/powerup.wav", Sound.class).play();
        }
        else {
            Hud.addScore(300);
        }
    }

    public boolean isBig() {
        return marioIsBig;
    }

    public boolean isDead() {
        return isMarioDead;
    }

    public float getStateTimer() {
        return stateTimer;
    }

    public void hit(Enemy enemy) {
        if(enemy instanceof Turtle && ((Turtle) enemy).getCurrentState() == Turtle.State.STANDING_SHELL) {
            ((Turtle) enemy).kick(this.getX() <= enemy.getX() ? Turtle.KICK_RIGHT_SPEED : Turtle.KICK_LEFT_SPEED);
        }
        else {
            if (marioIsBig) {
                marioIsBig = false;
                timeToRedefineMario = true;
                setBounds(getX(), getY(), getWidth(), getHeight() / 2);
                Mario.manager.get("audio/sounds/powerdown.wav", Sound.class).play();
            } else {
                killMario();
            }
        }
    }

    public void killMario() {
        Mario.manager.get("audio/music/mario_music.ogg", Music.class).stop();
        Mario.manager.get("audio/sounds/mariodie.wav", Sound.class).play();
        isMarioDead = true;
        Filter filter = new Filter();
        filter.maskBits = Mario.NOTHING_BIT;

        for (Fixture fixture : b2body.getFixtureList()) {
            fixture.setFilterData(filter);
        }

        b2body.applyLinearImpulse(new Vector2(0, 4f), b2body.getWorldCenter(), true);
    }

    private void redefineMario() {
        Vector2 currentPosition = b2body.getPosition();
        world.destroyBody(b2body);

        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(currentPosition);
        bodyDef.type = BodyDef.BodyType.DynamicBody;

        b2body = world.createBody(bodyDef);

        FixtureDef fixtureDef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(6 / Mario.PixelsPerMeter);
        fixtureDef.shape = shape;
        fixtureDef.filter.categoryBits = Mario.MARIO_BIT;
        fixtureDef.filter.maskBits = Mario.BRICK_BIT |
                Mario.COIN_BIT |
                Mario.GROUND_BIT |
                Mario.OBJECT_BIT |
                Mario.ENEMY_BIT |
                Mario.ENEMY_HEAD_BIT |
                Mario.ITEM_BIT |
                Mario.FINISH_BIT;

        b2body.createFixture(fixtureDef).setUserData(this);

        EdgeShape head = new EdgeShape();
        head.set(
                new Vector2(
                        -2 / Mario.PixelsPerMeter,
                        6 / Mario.PixelsPerMeter),
                new Vector2(
                        2 / Mario.PixelsPerMeter,
                        6 / Mario.PixelsPerMeter)
        );

        fixtureDef.shape = head;
        fixtureDef.isSensor = true;
        fixtureDef.filter.categoryBits = Mario.MARIO_HEAD_BIT;

        b2body.createFixture(fixtureDef).setUserData(this);

        timeToRedefineMario = false;
    }

    public void win() {
        hasWon = true;
    }

    public boolean hasWon() {
        return hasWon;
    }
}
