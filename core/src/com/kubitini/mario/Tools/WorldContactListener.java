package com.kubitini.mario.Tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.kubitini.mario.Mario;
import com.kubitini.mario.Sprites.Brick;
import com.kubitini.mario.Sprites.Enemy;
import com.kubitini.mario.Sprites.InteractiveTileObject;
import com.kubitini.mario.Sprites.Item;
import com.kubitini.mario.Sprites.MarioSprite;

/**
 * Created by Kubitini on 19. 12. 2017.
 */

public class WorldContactListener implements ContactListener {
    @Override
    public void beginContact(Contact contact) {
        Fixture fixA = contact.getFixtureA();
        Fixture fixB = contact.getFixtureB();

        int collisionDefinition = fixA.getFilterData().categoryBits | fixB.getFilterData().categoryBits;


        switch (collisionDefinition) {
            case Mario.ENEMY_HEAD_BIT | Mario.MARIO_BIT:
                if(fixA.getFilterData().categoryBits == Mario.ENEMY_HEAD_BIT) {
                    ((Enemy)fixA.getUserData()).onHitHead((MarioSprite) fixB.getUserData());
                }
                else {
                    ((Enemy)fixB.getUserData()).onHitHead((MarioSprite) fixA.getUserData());
                }
                break;
            case Mario.ENEMY_BIT | Mario.OBJECT_BIT:
                if(fixA.getFilterData().categoryBits == Mario.ENEMY_BIT) {
                    ((Enemy)fixA.getUserData()).reverseVelocity(true, false);
                }
                else {
                    ((Enemy)fixB.getUserData()).reverseVelocity(true, false);
                }
                break;
            case Mario.MARIO_BIT | Mario.ENEMY_BIT:
                if(fixA.getFilterData().categoryBits == Mario.MARIO_BIT)
                    ((MarioSprite) fixA.getUserData()).hit((Enemy)fixB.getUserData());
                else
                    ((MarioSprite) fixB.getUserData()).hit((Enemy)fixA.getUserData());
                break;
            case Mario.ENEMY_BIT:
                ((Enemy)fixA.getUserData()).onEnemyHit((Enemy) fixB.getUserData());
                ((Enemy)fixB.getUserData()).onEnemyHit((Enemy) fixA.getUserData());
                break;
            case Mario.ITEM_BIT | Mario.OBJECT_BIT:
                if(fixA.getFilterData().categoryBits == Mario.ITEM_BIT) {
                    ((Item)fixA.getUserData()).reverseVelocity(true, false);
                }
                else {
                    ((Item)fixB.getUserData()).reverseVelocity(true, false);
                }
                break;
            case Mario.ITEM_BIT | Mario.MARIO_BIT:
                if(fixA.getFilterData().categoryBits == Mario.ITEM_BIT) {
                    ((Item)fixA.getUserData()).use((MarioSprite) fixB.getUserData());
                }
                else {
                    ((Item)fixB.getUserData()).use((MarioSprite) fixA.getUserData());
                }
                break;
            case Mario.MARIO_HEAD_BIT | Mario.BRICK_BIT:
            case Mario.MARIO_HEAD_BIT | Mario.COIN_BIT:
                if(fixA.getFilterData().categoryBits == Mario.MARIO_HEAD_BIT) {
                    ((InteractiveTileObject) fixB.getUserData()).onHeadHit((MarioSprite) fixA.getUserData());
                    ((MarioSprite) fixA.getUserData()).currentState = MarioSprite.State.JUMPING;
                }
                else {
                    ((InteractiveTileObject) fixA.getUserData()).onHeadHit((MarioSprite) fixB.getUserData());
                    ((MarioSprite) fixB.getUserData()).currentState = MarioSprite.State.JUMPING;
                }
                break;
            case Mario.MARIO_BIT | Mario.FINISH_BIT:
                if(fixA.getFilterData().categoryBits == Mario.MARIO_BIT) {
                    ((MarioSprite)fixA.getUserData()).win();
                }
                else {
                    ((MarioSprite)fixB.getUserData()).win();
                }
                break;
        }
    }

    @Override
    public void endContact(Contact contact) {

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
