package com.kubitini.mario.Tools;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.kubitini.mario.Mario;
import com.kubitini.mario.Screens.PlayScreen;
import com.kubitini.mario.Sprites.Brick;
import com.kubitini.mario.Sprites.Coin;
import com.kubitini.mario.Sprites.Enemy;
import com.kubitini.mario.Sprites.FinishLine;
import com.kubitini.mario.Sprites.Goomba;
import com.kubitini.mario.Sprites.Turtle;

/**
 * Created by Kubitini on 9. 12. 2017.
 */

public class B2WorldCreator {
    private Array<Goomba> goombas;
    private Array<Turtle> turtles;
    private Array<FinishLine> finishLines;

    public B2WorldCreator(PlayScreen screen) {
        World world = screen.getWorld();
        TiledMap map = screen.getMap();

        BodyDef bodyDef = new BodyDef();
        PolygonShape shape = new PolygonShape();
        FixtureDef fixtureDef = new FixtureDef();
        Body body;

        // Ground bodies and fixtures
        for (MapObject object : map.getLayers().get(2).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();

            bodyDef.type = BodyDef.BodyType.StaticBody;
            bodyDef.position.set(
                    (rect.getX() + rect.getWidth() / 2) / Mario.PixelsPerMeter,
                    (rect.getY() + rect.getHeight() / 2) / Mario.PixelsPerMeter
            );

            body = world.createBody(bodyDef);

            shape.setAsBox(
                    (rect.getWidth() / 2) / Mario.PixelsPerMeter,
                    (rect.getHeight() / 2) / Mario.PixelsPerMeter
            );
            fixtureDef.shape = shape;
            body.createFixture(fixtureDef);
        }

        // Pipes bodies and fixtures
        for (MapObject object : map.getLayers().get(3).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();

            bodyDef.type = BodyDef.BodyType.StaticBody;
            bodyDef.position.set(
                    (rect.getX() + rect.getWidth() / 2) / Mario.PixelsPerMeter,
                    (rect.getY() + rect.getHeight() / 2) / Mario.PixelsPerMeter
            );

            body = world.createBody(bodyDef);

            shape.setAsBox(
                    (rect.getWidth() / 2) / Mario.PixelsPerMeter,
                    (rect.getHeight() / 2) / Mario.PixelsPerMeter
            );
            fixtureDef.shape = shape;
            fixtureDef.filter.categoryBits = Mario.OBJECT_BIT;
            body.createFixture(fixtureDef);
        }

        // Bricks bodies and fixtures
        for (MapObject object : map.getLayers().get(5).getObjects().getByType(RectangleMapObject.class)) {

            new Brick(screen, object);
        }

        // Coins bodies and fixtures
        for (MapObject object : map.getLayers().get(4).getObjects().getByType(RectangleMapObject.class)) {

            new Coin(screen, object);
        }

        // Goombas
        goombas = new Array<Goomba>();
        for (MapObject object : map.getLayers().get(6).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();

            goombas.add(
                    new Goomba(
                            screen,
                            rect.getX() / Mario.PixelsPerMeter,
                            rect.getY() / Mario.PixelsPerMeter
                    )
            );
        }

        // Turtles
        turtles = new Array<Turtle>();
        for (MapObject object : map.getLayers().get(7).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();

            turtles.add(
                    new Turtle(
                            screen,
                            rect.getX() / Mario.PixelsPerMeter,
                            rect.getY() / Mario.PixelsPerMeter
                    )
            );
        }

        // Finish lines
        finishLines = new Array<FinishLine>();
        for (MapObject object : map.getLayers().get(8).getObjects().getByType(RectangleMapObject.class)) {

            finishLines.add(new FinishLine(screen, object));
        }
    }

    public Array<Goomba> getGoombas() {
        return goombas;
    }

    public Array<Enemy> getEnemies() {
        Array<Enemy> enemies = new Array<Enemy>();
        enemies.addAll(goombas);
        enemies.addAll(turtles);

        return enemies;
    }
}
