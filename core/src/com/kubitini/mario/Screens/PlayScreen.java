package com.kubitini.mario.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.kubitini.mario.Mario;
import com.kubitini.mario.Scenes.Controller;
import com.kubitini.mario.Scenes.Hud;
import com.kubitini.mario.Sprites.Enemy;
import com.kubitini.mario.Sprites.Goomba;
import com.kubitini.mario.Sprites.Item;
import com.kubitini.mario.Sprites.ItemDefinition;
import com.kubitini.mario.Sprites.MarioSprite;
import com.kubitini.mario.Sprites.Mushroom;
import com.kubitini.mario.Tools.B2WorldCreator;
import com.kubitini.mario.Tools.WorldContactListener;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by Kubitini on 9. 12. 2017.
 */

public class PlayScreen implements Screen {
    private Mario game;
    private OrthographicCamera camera;
    private Viewport gamePort;

    private Hud hud;
    private Controller controller;

    private TmxMapLoader mapLoader;
    private TiledMap map;
    private OrthogonalTiledMapRenderer renderer;

    private World world;
    private Box2DDebugRenderer b2dr;
    private B2WorldCreator creator;

    private MarioSprite player;

    private TextureAtlas atlas;

    private Music music;

    private Array<Item> items;
    private LinkedBlockingQueue<ItemDefinition> itemsToSpawn;

    public PlayScreen(Mario game) {
        atlas = new TextureAtlas("Mario_and_Enemies.pack");

        this.game = game;
        camera = new OrthographicCamera();
        gamePort = new FitViewport(
                Mario.V_WIDTH / Mario.PixelsPerMeter,
                Mario.V_HEIGHT / Mario.PixelsPerMeter,
                camera
        );

        mapLoader = new TmxMapLoader();
        map = mapLoader.load("level1.tmx");
        renderer = new OrthogonalTiledMapRenderer(map, 1 / Mario.PixelsPerMeter);

        camera.position.set(
                gamePort.getWorldWidth() / 2,
                gamePort.getWorldHeight() / 2,
                0
        );

        world = new World(new Vector2(0, -10), true);
        b2dr = new Box2DDebugRenderer();

        creator = new B2WorldCreator(this);

        player = new MarioSprite(this);

        hud = new Hud(game.batch, player);
        controller = new Controller(game.batch);

        world.setContactListener(new WorldContactListener());

        music = Mario.manager.get("audio/music/mario_music.ogg", Music.class);
        music.setLooping(true);
        // TODO: Zapnout na produkci hlavní znělku
        music.play();

        items = new Array<Item>();
        itemsToSpawn = new LinkedBlockingQueue<ItemDefinition>();
    }

    public void spawnItem(ItemDefinition itemDef) {
        itemsToSpawn.add(itemDef);
    }

    public void handleSpawningItems() {
        if(!itemsToSpawn.isEmpty()) {
            ItemDefinition itemDefinition = itemsToSpawn.poll();

            if(itemDefinition.type == Mushroom.class) {
                items.add(
                        new Mushroom(
                                this,
                                itemDefinition.position.x,
                                itemDefinition.position.y
                        )
                );
            }
        }
    }

    public TextureAtlas getAtlas() {
        return atlas;
    }

    private void handleInput(float dt) {
        if(player.currentState != MarioSprite.State.DEAD) {
            if(Gdx.input.isKeyJustPressed(Input.Keys.UP) || (Gdx.input.justTouched() && controller.isJumpPressed())) {
                if(player.getState() != MarioSprite.State.JUMPING) {
                    player.b2body.applyLinearImpulse(new Vector2(0, 4f), player.b2body.getWorldCenter(), true);

                    if (player.isBig()) {
                        Mario.manager.get("audio/sounds/jump_big.wav", Sound.class).play();
                    } else {
                        Mario.manager.get("audio/sounds/jump_small.wav", Sound.class).play();
                    }
                }
            }
            if((Gdx.input.isKeyPressed(Input.Keys.RIGHT) || controller.isRightPressed()) && player.b2body.getLinearVelocity().x <= 2) {
                player.b2body.applyLinearImpulse(new Vector2(0.1f, 0), player.b2body.getWorldCenter(), true);
            }
            if((Gdx.input.isKeyPressed(Input.Keys.LEFT) || controller.isLeftPressed()) && player.b2body.getLinearVelocity().x >= -2) {
                player.b2body.applyLinearImpulse(new Vector2(-0.1f, 0), player.b2body.getWorldCenter(), true);
            }
        }
    }

    public World getWorld() {
        return world;
    }

    public TiledMap getMap() {
        return map;
    }

    private void update(float dt) {
        handleInput(dt);
        handleSpawningItems();

        world.step(1/60f, 6, 2);
        if(player.currentState != MarioSprite.State.DEAD) {
            camera.position.x = player.b2body.getPosition().x;
        }

        player.update(dt);

        for (Enemy enemy : creator.getEnemies()) {
            enemy.update(dt);

            if(enemy.getX() < player.getX() + 224 / Mario.PixelsPerMeter) {
                enemy.b2body.setActive(true);
            }
        }

        for(Item item : items) {
            item.update(dt);
        }
        camera.update();
        renderer.setView(camera);

        hud.update(dt);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        update(delta);

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        renderer.render();
        //b2dr.render(world, camera.combined);

        game.batch.setProjectionMatrix(camera.combined);
        game.batch.begin();
        player.draw(game.batch);

        for (Enemy enemy : creator.getEnemies()) {
            enemy.draw(game.batch);
        }

        for(Item item : items) {
            item.draw(game.batch);
        }

        game.batch.end();

        game.batch.setProjectionMatrix(hud.stage.getCamera().combined);
        hud.stage.draw();

        game.batch.setProjectionMatrix(controller.getCamera().combined);
        controller.draw();

        if(isGameOver()) {
            game.setScreen(new GameOverScreen(game));
            dispose();
        }

        if(player.hasWon()) {
            game.setScreen(new FinishScreen(game));
            dispose();
        }
    }

    public boolean isGameOver() {
        if(player.currentState == MarioSprite.State.DEAD && player.getStateTimer() > 3) {
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public void resize(int width, int height) {
        gamePort.update(width, height);
        controller.resize(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        map.dispose();
        renderer.dispose();
        world.dispose();
        b2dr.dispose();
        hud.dispose();
        controller.dispose();
    }
}
